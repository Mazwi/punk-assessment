import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const MyComponent = () => (
  <RaisedButton label="Install ..." />
);

export default MyComponent