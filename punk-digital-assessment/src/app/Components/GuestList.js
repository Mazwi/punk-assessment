import React from 'react';
import {List, ListItem} from 'material-ui/List';
import SocialPerson from 'material-ui/svg-icons/social/person';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';

const guestList = require('xml!../guestList.xml')

const guests = 
    guestList.dataset.record.map(function(guest, i){
	  return <ListItem primaryText={guest.first_name[0] + ' ' + guest.last_name[0]} leftAvatar={<Avatar icon={<SocialPerson />} />} />
	})
  
const GuestList = () => (
	<List>
	  {guests}
	</List>
);

export default GuestList;