const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, 'build');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');

const config = {
  // Entry points to the project
	entry: './src/app/index.js',
	devServer: {
		contentBase: 'src/www', // Relative directory for base of server
		inline: true,
		port: 8087 // Port Number
	},
	output: {
	path: __dirname,
    publicPath: '/build/', // Path of output file
    filename: 'app.js',
  },
  module: {
    loaders: [
      {
        test: /\.js$/, // All .js files
        loaders: ['babel-loader'],
		loader: 'xml-loader',
        exclude: [nodeModulesPath],
      },
    ]
  },
};

module.exports = config;
